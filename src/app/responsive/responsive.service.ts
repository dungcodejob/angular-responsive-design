import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  BreakpointType,
  DeviceType,
  OrientationType,
  ScreenSizeType,
} from './responsive-enum';
@Injectable({
  providedIn: 'root',
})
export class ResponsiveService {
  private _screenSizeSubj = new BehaviorSubject(ScreenSizeType.Unknown);
  private _deviceTypeSubj = new BehaviorSubject(DeviceType.Unknown);
  private _orientationSubj = new BehaviorSubject(OrientationType.Unknown);

  get screenSize$(): Observable<ScreenSizeType> {
    return this._screenSizeSubj.asObservable();
  }

  get screenSize(): ScreenSizeType {
    return this._screenSizeSubj.value;
  }

  get deviceType$(): Observable<DeviceType> {
    return this._deviceTypeSubj.asObservable();
  }

  get deviceType(): DeviceType {
    return this._deviceTypeSubj.value;
  }

  get orientation$(): Observable<OrientationType> {
    return this._orientationSubj.asObservable();
  }

  get orientation(): OrientationType {
    return this._orientationSubj.value;
  }

  private readonly screenSizeBreakpoint = new Map([
    [Breakpoints.XSmall, ScreenSizeType.XSmall],
    [Breakpoints.Small, ScreenSizeType.Small],
    [Breakpoints.Medium, ScreenSizeType.Medium],
    [Breakpoints.Large, ScreenSizeType.Large],
    [Breakpoints.XLarge, ScreenSizeType.XLarge],
  ]);

  private readonly deviceAndOrientation = new Map([
    [Breakpoints.HandsetLandscape, BreakpointType.HandsetLandscape],
    [Breakpoints.HandsetPortrait, BreakpointType.HandsetPortrait],
    [Breakpoints.TabletLandscape, BreakpointType.TabletLandscape],
    [Breakpoints.TabletPortrait, BreakpointType.TabletPortrait],
    [Breakpoints.WebLandscape, BreakpointType.WebLandscape],
    [Breakpoints.WebPortrait, BreakpointType.WebPortrait],
  ]);

  constructor(breakpointObserver: BreakpointObserver) {
    this.checkScreenSize(breakpointObserver);
    this.checkDeviceAndOrientation(breakpointObserver);
  }

  orientationPortrait(): boolean {
    return this.orientation === OrientationType.Portrait;
  }
  orientationLandscape(): boolean {
    return this.orientation === OrientationType.Landscape;
  }

  deviceDesktop(): boolean {
    return this.deviceType === DeviceType.Web;
  }
  deviceTablet(): boolean {
    return this.deviceType === DeviceType.Tablet;
  }
  deviceMobile(): boolean {
    return this.deviceType === DeviceType.Handset;
  }

  private checkScreenSize(breakpointObserver: BreakpointObserver) {
    breakpointObserver
      .observe([
        Breakpoints.XSmall,
        Breakpoints.Small,
        Breakpoints.Medium,
        Breakpoints.Large,
        Breakpoints.XLarge,
      ])
      .subscribe((result) => {
        for (const key of Object.keys(result.breakpoints)) {
          if (result.breakpoints[key]) {
            this._screenSizeSubj.next(
              this.screenSizeBreakpoint.get(key) ?? ScreenSizeType.Unknown
            );
          }
        }
      });
  }

  private checkDeviceAndOrientation(breakpointObserver: BreakpointObserver) {
    breakpointObserver
      .observe([
        Breakpoints.HandsetLandscape,
        Breakpoints.HandsetPortrait,
        Breakpoints.WebLandscape,
        Breakpoints.WebPortrait,
        Breakpoints.TabletLandscape,
        Breakpoints.TabletPortrait,
      ])
      .subscribe((result) => {
        let orientationKeys = Object.keys(OrientationType).map((key) => key);
        let deviceKeys = Object.keys(DeviceType).map((key) => key);

        for (const key of Object.keys(result.breakpoints)) {
          if (result.breakpoints[key]) {
            let breakpointType =
              this.deviceAndOrientation.get(key) ?? BreakpointType.Unknown;

            orientationKeys.forEach((oKey) => {
              if (breakpointType.indexOf(oKey) !== -1) {
                this._orientationSubj.next(oKey as OrientationType);
              }
            });

            deviceKeys.forEach((dKey) => {
              if (breakpointType.indexOf(dKey) !== -1) {
                this._deviceTypeSubj.next(dKey as DeviceType);
              }
            });
          }
        }
      });
  }
}
