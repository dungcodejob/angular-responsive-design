
export enum BrowserType {
  Opera = 'Opera',
  Firefox = 'Firefox',
  InternetExplorer = 'Internet Explorer',
  Edge = 'Microsoft Edge',
  Chrome = 'Google Chrome or Chromium',
  Safari = 'Safari',
  Unknown = 'unknown',
}


