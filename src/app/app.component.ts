import { Component, inject } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { ResponsiveService } from './responsive';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  private readonly _responsiveService = inject(ResponsiveService);


  screenSize = toSignal(this._responsiveService.screenSize$, {
    requireSync: true,
  });
  deviceType = toSignal(this._responsiveService.deviceType$, {
    requireSync: true,
  });
  orientation = toSignal(this._responsiveService.orientation$, {
    requireSync: true,
  });

  isMobile() {
    return this._responsiveService.deviceMobile();
  }

  isDesktop() {
    return this._responsiveService.deviceDesktop();
  }

  isTablet() {
    return this._responsiveService.deviceTablet();
  }
}
